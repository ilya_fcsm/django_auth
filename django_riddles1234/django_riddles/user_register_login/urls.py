from django.conf.urls import url
from django.urls import path
# import views from local directory.
from . import views

urlpatterns = [
    # Не используйте пустую строку в пути запроса, иначе будут перехвачены все url-адреса запросов со всем значением пути запроса
    # url(r'', views.home_page, name='home_page'),
    # если вы хотите перехватить запрос корневого пути приложения django, просто используйте url-адрес R'^$', чтобы отобразить его.
    # Следующее сопоставление url-адресов будет обрабатывать запрос http://127.0.0.1:8000/ с функцией просмотра home_page.
    url(r'^$', views.home_page, name='home_page'),
    # Обращение к url http://127.0.0.1:8000/user/home/
    url(r'^home/$', views.home_page, name='home_page'),
    #Просмотр пользователем http://localhost:8000/user/do_login вызовет функцию do_login, определенную во views.py.
    url(r'^do_login/$', views.do_login, name='do_login'),
    # Обращение к url http://127.0.0.1:8000/user/login_success/, этот url-адрес будет вызван после успешного входа в систему.
    url(r'^login_success/$', views.login_success, name='login_success'),
    # Обращение к url http://127.0.0.1:8000/user/register/
    url(r'^register/$', views.user_register, name='user_register'),
    # При просмотре пользователем http://localhost:8000/user/do_register, будет вызвана функция do_register, описанная во views.py.
    url(r'^do_register/$', views.do_register, name='do_register'),
    # При обращении к url http://127.0.0.1:8000/user/register_success/, этот url-адрес будет вызван после успешной регистрации в системе.
    url(r'^register_success/$', views.register_success, name='register_success'),
]