import json

from django.shortcuts import render
from django.http import HttpResponseRedirect

from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.decorators import api_view, permission_classes
from rest_framework.generics import RetrieveUpdateAPIView

from rest_framework_jwt.utils import jwt_payload_handler, jwt_encode_handler

from django.contrib.auth.models import User
from django.contrib.auth import authenticate


@permission_classes([AllowAny, ])
def home_page(request):
    return render(request, 'user_login.html')

def do_login(request):

    request_method = request.method

    print('request_method = ' + request_method)

    if request_method == 'POST':
        user_name = request.POST.get('user_name','')
        user_password = request.POST.get('user_password', '')

        user = authenticate(username=user_name, password=user_password)

        if  user is not None:

            payload = jwt_payload_handler(user)
            token = jwt_encode_handler(payload)
            
            user_details = {}
            user_details['username'] = user_name
            user_details['token'] = token

            # Перенаправление страницы на страницу login_success.
            response = HttpResponseRedirect('/user/login_success/')
            print('===================== response')
            print(response)
            # Установка cookie для передачи имени пользователя на страницу login_success.
            response.set_cookie('token', token, 3600)
            response.set_cookie('user_name', user_name, 3600)
            return response
        else:
            error_json = {'error_message' : 'Имя пользователя или пароль не корректны!'}
            return render(request, 'user_login.html', error_json)
    else:
        return render(request, 'user_login.html')

def login_success(request):
    #Получение имени пользователя из cookie.
    token = request.COOKIES.get('token','')
    user_name = request.COOKIES.get('user_name','')
    
    # Передача имени пользователя для отображения страницы.
    return render(request, 'user_login_success.html', {'user_name':user_name})

def user_register(request):
    # Очистка значения сеанса перед регистрацией.
    request.session['user_name'] = ''
    request.session['user_password'] = ''
    request.session['user_email'] = ''
    return render(request, 'user_register.html', {'user_name':'','user_password':'','user_email':''})

def do_register(request):
    request_method = request.method

    print('request_method = ' + request_method)

    if request_method == 'POST':
        user_name = request.POST.get('user_name', '')
        user_password = request.POST.get('user_password', '')
        user_email = request.POST.get('user_email', '')

        if len(user_name) > 0 and len(user_password) > 0 and len(user_email) > 0:
            # Перенаправление страницы на страницу register_success.
            response = HttpResponseRedirect('/user/register_success/')
            # Установка имени пользователя, пароля и почты.
            request.session['user_name'] = user_name
            request.session['user_password'] = user_password
            request.session['user_email'] = user_email
            return response
        else:
            error_json = {'error_message': 'User name, password and email can not be empty.'}
            return render(request, 'user_register.html', error_json)
    else:
        return render(request, 'user_register.html')


def register_success(request):
    # Получение значений имени пользователя, пароля и почты из сессии.
    user_name = request.session.get('user_name','')
    user_password = request.session.get('user_password', '')
    user_email = request.session.get('user_email', '')
    # pПередача имени пользователя, пароля и почты для отображения веб-страницы.
    return render(request, 'user_register_success.html', {'user_name':user_name,'user_password':user_password,'user_email':user_email})